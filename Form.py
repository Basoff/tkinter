from tkinter import *
from tkinter.ttk import Radiobutton

#Запись инфы в файл
def submit():
    entry = name.get() + ' ' + surname.get() + ', ' + gender.get()
    output.write(entry + '\n')
    output.close()
    window.destroy()

output = open('Out.txt', 'a')

#Инициализация окна
window = Tk()
window.title('Autorize')
window.geometry('640x480')

#Переменная для радиокнопки
gender = StringVar()

#Окно ввода имени
name = Entry(window, width=20)
name.place(x=10, y=10)

#Окно ввода фамилии
surname = Entry(window, width=20)
surname.place(x=140, y=10)

#Радиокнопки
rad_male = Radiobutton(window, text="Male", value='Male', variable=gender)
rad_male.place(x=10, y=30)
rad_female = Radiobutton(window, text="Female", value='Female', variable=gender)
rad_female.place(x=60, y=30)

#Кнопка отправки инфы
submit_button = Button(window, text='Submit', command=submit)
submit_button.place(x=10, y=50)

#Запуск окна
window.mainloop()
